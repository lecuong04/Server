var Express = require("express");
var Fs = require("fs");
var Bp = require("body-parser");
var App = Express();

App.use(Bp.json());
App.use(Bp.urlencoded({ extended: true }));
var Port = 3000;
var Data = JSON.parse(Fs.readFileSync("databases.json"));

function WriteJSON(Content) {
  Fs.writeFile("databases.json", JSON.stringify(Content), "utf8", function (
    Error
  ) {
    if (Error) {
      console.log("An error occured while writing JSON Object to File.");
      console.log(Error);
    }
  });
}

App.get("/", (Req, Res) => {
  Res.send();
});

App.get("/get", (Req, Res) => {
  Res.json(Data);
});

App.post("/post", (Req, Res) => {
  var Raw = {
    Computer: Req.body.Computer,
    User: Req.body.User,
    Command: Req.body.Command
  };
  if (
    (Req.body.Computer == null) &
    (Req.body.User == null) &
    (Req.body.Command == null)
  ) {
    Res.send("Failed!");
  } else {
    if (Data !== "[]") {
      if (JSON.stringify(Raw) !== JSON.stringify(Data[Data.length - 1])) {
        Data.push(Raw);
        WriteJSON(Data);
      }
    }
    Res.send("OK!");
  }
});

App.post("/delete", (Req, Res) => {
  if (Req.body.Detele != null) {
    Data.splice(Req.body.Detele, 1);
    WriteJSON(Data);
    Res.send("OK!");
  } else {
    Res.send("Failed!");
  }
});

App.post("/clear", (Req, Res) => {
  Data = [];
  WriteJSON(Data);
  Res.send("OK!");
});

App.listen(Port, () => {
  console.log("Server running on port " + Port);
});
